import { useHistory } from 'react-router-dom';
import { useSelector } from "react-redux";

import Product from '../../components/Product';

const Cart = () => {
  const history = useHistory();
  const cart = useSelector((state) => state.cart);
  return (
    <div>
      <div>
        <h1>Meu carrinho</h1>
        <label>Total - {cart.reduce((acc, product) => acc + product.price, 0)}</label>
        <button onClick={() => history.push("/")}>Voltar</button>
      </div>
      <div>
        {cart.map((product, index) => (
          <Product key={index} product={product} isRemovable />
        ))}
      </div>
    </div>
  );
};

export default Cart;
