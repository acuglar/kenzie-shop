import { Container } from './styles'

import LinkedInIcon from '@material-ui/icons/LinkedIn';

const Footer = () => {
  return (
    <Container>
      <div></div>
      <LinkedInIcon fontSize="large" color="primary" />
      <p>Kenzie Shop</p>
    </Container>
  );
};
export default Footer;
