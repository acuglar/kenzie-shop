import styled from 'styled-components';

export const Container = styled.footer`
background-color: #003067;
position: fixed;
left: 0;
bottom: 0;
width: 100%;
text-align: center;
`;