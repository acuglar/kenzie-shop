import {
  Header,
  Title,
  Picture
} from './styles'

import { useHistory } from 'react-router-dom';

const Menu = () => {
  const history = useHistory();

  const sendTo = (path) => {
    history.push(path);
  };

  return (
    <Header>
      <Picture>
        <img src="../../images/KenzieLogo.jpg" alt="KenzieLogo" />
        <Title>Kenzie Shop</Title>
      </Picture>
      <div>
        <button onClick={() => sendTo("/")}>HOME</button>
        <button onClick={() => sendTo("/cart")}>CART</button>
      </div>
    </Header>
  )
}

export default Menu;