import styled from 'styled-components';

export const Header = styled.header`
background-color: #F7F7F7;
position: fixed;
top: 0;
width: 100%;
display: flex;
justify-content: space-between;
height: 18vh;
`;

export const KenzieLogo = styled.img`
  height: 20px;
`;

export const Title = styled.h1`
margin: 20px;
color: #003067;
`;

export const Picture = styled.picture`
width: 100%;
border: 1px solid red;
`;