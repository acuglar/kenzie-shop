import { useDispatch } from 'react-redux';
import { addToCartThunk, removeFromCartThunk } from '../../store/modules/cart/thunks';

const Product = ({ product, isRemovable = false }) => {
  const dispatch = useDispatch();

  return (
    <div>
      <h3>{product.name}</h3>
      <h2>{product.price}</h2>
      {isRemovable ? (
        <button onClick={() => dispatch(removeFromCartThunk(product.id))}>
          Remover
        </button>
      ) : (
          <button onClick={() => dispatch(addToCartThunk(product))}>
            Adicionar ao carrinho
          </button>
        )}
    </div>
  );
};

export default Product;