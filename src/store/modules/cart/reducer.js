const cartReducer = (state = [], action) => {
  //inserir lógica para verificar os dados do localStorage.
  //a partir disso verifica se inicia cart vazio ou com os dados da storage
  console.log('reduce', action)
  switch (action.type) {
    case "@cart/ADD":
      const { product } = action;
      return [...state, product];

    case "@cart/REMOVE":
      const { list } = action;
      return list;

    default:
      return state;
  }
};

export default cartReducer;

