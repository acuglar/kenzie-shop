import { addToCart, removeFromCart } from "./actions";

export const addToCartThunk = (product) => {
  return async (dispatch, getState) => {
    const list = JSON.parse(localStorage.getItem("cart")) || [];
    list.push(product);
    localStorage.setItem("cart", JSON.stringify(list));
    dispatch(addToCart(product));
  };
};

export const removeFromCartThunk = (id) => (dispatch, getState) => {
  const { cart } = getState();
  const list = cart.filter((product) => product.id !== id);
  localStorage.setItem("cart", JSON.stringify(list));
  dispatch(removeFromCart(list));
};
