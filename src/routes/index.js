import Home from "../pages/Home";
import Cart from "../pages/Cart";

import { Switch, Route } from "react-router-dom";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact pach="/cart">
        <Cart />
      </Route>
    </Switch>
  );
};
export default Routes;