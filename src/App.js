import Menu from './components/Menu';
import Routes from './routes';
import Footer from './components/Footer';

import './App.css';

function App() {
  return (
    <div className="App">
      <Menu />
      <Routes />
      <Footer />
    </div>
  );
}

export default App;
